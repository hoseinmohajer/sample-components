import React, { useState } from 'react';
import './style.scss';

export default ({ items }) => {
  const [status, setStatus] = useState(false);
  const [id, setId] = useState(null);
  const toggleMenu = (key) => {
    if (Number(key) === Number(id)) {
      setStatus(!status);
    } else {
      setStatus(true);
    }
    setId(key);
  };

  return (
    <div className='accordion-wrapper'>
      {items.map((item, key) => {
        const show = (status && Number(id) === Number(key)) ? true : undefined;
        return (
          <div className='accordion-row' key={key}>
            <div className='header' onClick={() => toggleMenu(key)}>
              <h1 className='accordion-title'>{item.title}</h1>
              <div className="icon" show={show}>
                <i className={`${(show ? 'show' : 'hide')} accordion-icon icon-down-open-big`} />
              </div>
            </div>
            {show && (
              <div className="body">
                <ul className="accordion-ul">
                  {item.description.map((itm, key) => {
                    return (
                      <li key={key} className="accordion-li">
                        <div className="key">{itm.key.toString()}: </div>
                        <div className="value">{itm.value.toString()}</div>
                      </li>
                    );
                  })}
                </ul>
              </div>
            )}
          </div>
        );
      })}
    </div>
  );
};
