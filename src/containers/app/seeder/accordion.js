export default [
  {
    title: 'One',
    description: [
      {
        key: 'foo',
        value: 'Lorem ipsum dolor sit amet, consectetur!',
      },
    ],
  },
  {
    title: 'two',
    description: [
      {
        key: 'foo',
        value: 'Lorem ipsum dolor sit amet, consectetur!',
      },
      {
        key: 'foo',
        value: 'Lorem ipsum dolor sit amet, consectetur!',
      },
      {
        key: 'foo',
        value: 'Lorem ipsum dolor sit amet, consectetur!',
      },
      {
        key: 'foo',
        value: 'Lorem ipsum dolor sit amet, consectetur!',
      },
      {
        key: 'foo',
        value: 'Lorem ipsum dolor sit amet, consectetur!',
      },
    ],
  },
  {
    title: 'three',
    description: [
      {
        key: 'foo',
        value: 'Lorem ipsum dolor sit amet, consectetur!',
      },
    ],
  },
  {
    title: 'four',
    description: [
      {
        key: 'foo',
        value: 'Lorem ipsum dolor sit amet, consectetur!',
      },
    ],
  },
]